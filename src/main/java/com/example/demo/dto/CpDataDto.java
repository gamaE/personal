package com.example.demo.dto;

import java.util.ArrayList;
import java.util.List;

public class CpDataDto {

	private String zipCode;
	private String locality;
	private String federalEntity;
	private List<SettlementDto> settlements = new ArrayList<>();
	private String municipality;
	
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getFederalEntity() {
		return federalEntity;
	}
	public void setFederalEntity(String federalEntity) {
		this.federalEntity = federalEntity;
	}
	public List<SettlementDto> getSettlements() {
		return settlements;
	}
	public void setSettlements(List<SettlementDto> settlements) {
		this.settlements = settlements;
	}
	public String getMunicipality() {
		return municipality;
	}
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}
	
}
