package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CpDataDto;
import com.example.demo.service.ISearchService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/zip-codes")
public class SearchController {
	
	@Autowired
	ISearchService searchService;
	
	@GetMapping(value = "/{zip-code}")
	public ResponseEntity<CpDataDto> getCpData(
				@PathVariable(value = "zip-code", required = true) String zipCode) {
				
		CpDataDto cpData = searchService.getCpData(zipCode);
				
		if(cpData != null) {
			return new ResponseEntity<>(cpData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
  	
	}	

}
