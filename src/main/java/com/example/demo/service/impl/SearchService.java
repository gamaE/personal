package com.example.demo.service.impl;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.example.demo.dto.CpDataDto;
import com.example.demo.dto.SettlementDto;
import com.example.demo.service.ISearchService;

@Service
public class SearchService implements ISearchService {

	@Override
	public CpDataDto getCpData(String cp) {
		
		CpDataDto cpData = null;
		
		try {
			Resource resource = new ClassPathResource("ciudadDeMexico.xml");
			File xmlFile = resource.getFile();

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("table");
			
			int total = 0;

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
				
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					String readedCode = eElement.getElementsByTagName("d_codigo").item(0).getTextContent();  
					
					if(readedCode.equals(cp)) {
						
						if(++total == 1) {
							cpData = new CpDataDto();
							cpData.setZipCode(readedCode);
							cpData.setLocality(eElement.getElementsByTagName("d_estado").item(0).getTextContent());
							cpData.setFederalEntity(eElement.getElementsByTagName("d_ciudad").item(0).getTextContent());
							cpData.setMunicipality(eElement.getElementsByTagName("D_mnpio").item(0).getTextContent());
						}
						
						SettlementDto settlement = new SettlementDto();
						settlement.setName(eElement.getElementsByTagName("d_asenta").item(0).getTextContent());
						settlement.setZoneType(eElement.getElementsByTagName("d_zona").item(0).getTextContent());
						settlement.setSettlementType(eElement.getElementsByTagName("d_tipo_asenta").item(0).getTextContent());
						
						cpData.getSettlements().add(settlement);
						
					}
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cpData;
	}

}
