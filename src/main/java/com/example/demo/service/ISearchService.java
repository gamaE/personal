package com.example.demo.service;

import com.example.demo.dto.CpDataDto;

public interface ISearchService {
	
	public CpDataDto getCpData(String cp);

}
